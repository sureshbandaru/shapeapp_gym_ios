//
//  LoginViewController.swift
//  ShapeApp_Gym
//
//  Created by Satheesh Kumar on 24/01/19.
//  Copyright © 2019 Unique Angle. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    var access_token:String!

    
    
    @IBOutlet weak var EmailFld: HoshiTextField!
    @IBOutlet weak var PasswordFld: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let stap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismisskeyboard))
        self.view.addGestureRecognizer(stap)
        
        // Do any additional setup after loading the view.
    }
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func dismisskeyboard() {
        
        self.view.endEditing(true)
    }
    
    @IBAction func login () {
        
        if EmailFld.text?.count ?? 0 > 0 && PasswordFld.text?.count ?? 0 > 0 {
            
            let LatLon:NSDictionary = NSDictionary(objects: [EmailFld.text,PasswordFld.text], forKeys: ["email" as NSCopying,"password" as NSCopying])
            
            let respData = Appcontrol.common_http_request("/gym/login", value: LatLon.jsonRepresentation(), type:POST_METHOD, isjson: true,loader: true)
            
            if respData.jsonValue() != nil {
                if let respdict: NSDictionary = respData.jsonValue()! as? NSDictionary{
                    if respdict["access_token"] != nil {
                        
                        
                        //-------------------------changes made by suresh-------------------//
                        
                        self.access_token = respdict["access_token"] as! String
                        
                        print("Token",access_token)

                       
                        UserDefaults.standard.set(respdict, forKey: "API_TOKEN")
                        UserDefaults.standard.set(Date(), forKey: "TOKEN_REG_TIME")
                        UserDefaults.standard.set(true, forKey: "LOGGEDIN")
                        UserDefaults.standard.synchronize()
                        Appcontrol.refresh_user_data()
                        
                        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let rootVC = storyBoard.instantiateViewController(withIdentifier:"tabbarcontroller" )
                        let appDelegate = UIApplication.shared.delegate
                        appDelegate?.window??.rootViewController = rootVC
                    }else {
                        Appcontrol.shakeView(PasswordFld);
                        
                    }
                
            }

            }
        }else {
            
            let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let rootVC = storyBoard.instantiateViewController(withIdentifier:"tabbarcontroller" )
            let appDelegate = UIApplication.shared.delegate
            appDelegate?.window??.rootViewController = rootVC
            
        }
    
    }
    
    

    
}
