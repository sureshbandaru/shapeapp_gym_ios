//
//  ProfieEditViewController.swift
//  ShapeApp
//
//  Created by Satheesh Kumar on 08/12/18.
//  Copyright © 2018 UniqueAngels. All rights reserved.
//

import UIKit

class ProfieEditViewController: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var doneBarButton: UIBarButtonItem!
    @IBOutlet weak var firstnameTxtFld: UITextField!
    @IBOutlet weak var lastnameTxtFld: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        let userdata = Appcontrol.getuserdata();
        firstnameTxtFld.text = "\(userdata["first_name"] ?? "Guest")"
        lastnameTxtFld.text = "\(userdata["last_name"] ?? "")"

    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationItem.rightBarButtonItem?.setTitlePositionAdjustment(.init(horizontal: 0, vertical: -160), for: UIBarMetrics.default)
    }
    
    @IBAction func DismissVC(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updateName(_ sender: Any) {
        
        if (firstnameTxtFld.text?.count)! > 0 {
        
            let respData = Appcontrol.common_http_request("/change-username", value: "{\"first_name\":\"\(firstnameTxtFld.text ?? "Guest")\",\"last_name\":\"\(lastnameTxtFld.text ?? "")\"}", type:POST_METHOD, isjson: true, loader: true)
            
            if respData.jsonValue() != nil {
                if let userDetailsDictionary: NSDictionary = respData.jsonValue()! as? NSDictionary{
                    
                    if let code:Int = userDetailsDictionary.value(forKey: "status") as? Int {
                        if code == 200 {
                            Appcontrol.refresh_user_data()
                            self.dismiss(animated: true, completion: nil)
                        }
                    }
                }
                
            }
        }
        
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        doneBarButton.isEnabled = true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }

}
