//
//  FirstViewController.swift
//  ShapeApp_Gym
//
//  Created by Satheesh Kumar on 22/01/19.
//  Copyright © 2019 Unique Angle. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    var gymArray:NSMutableArray = NSMutableArray()
    
    var gymhoursArray:NSMutableArray = NSMutableArray()
    
    var gymidString = ""
    var idstr = ""
    
    
    var Anamicastr = ""

    
    
    let cellReuseIdentifier = "cell"
    
    
    
    
    @IBOutlet weak var ShapeAppToolbarview: UIView!
    
    @IBOutlet weak var drpdownview: UIView!
    
    @IBOutlet weak var Multipleselectiondrpdwntxt: UILabel!
    @IBOutlet weak var drpdwntableview: UITableView!
    
    @IBOutlet weak var WaitingListUser: UIView!
    
    @IBOutlet weak var WaitingListUserTextview: UITextView!
    
    
    @IBOutlet weak var CurrentUserView: UIView!
    
    @IBOutlet weak var CurrentUserTextView: UITextView!
   
    
    
    @IBOutlet weak var EarnedhoursView: UIView!
    
    
    @IBOutlet weak var EarnedhoursTextview: UITextView!
    
    
    
    
    @IBOutlet weak var GymInfoview: UIView!
    

    @IBOutlet weak var CurrentUserLbl: UILabel!
    @IBOutlet weak var WaitingUserLbl: UILabel!
    @IBOutlet weak var EarnedHoursLbl: UILabel!
    @IBOutlet weak var GymName: UIButton!
    @IBOutlet weak var QRCodeImgVw: UIImageView!
    
    
    

    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
        //-----------------changes made by suresh----------------------//
        
        
        
       // drpdownview.isHidden = true
        drpdwntableview.isHidden = true
        
        // Do any additional setup after loading the view, typically from a nib.
   
        self.drpdwntableview.register((UINib (nibName: "MultipleselectiondrpdwnCell", bundle: nil)), forCellReuseIdentifier: "cellId")
        
        Appcontrol.refresh_gym_data()
        
        print("Refreshdata",Appcontrol.refresh_gym_data)

        
//        self.ShapeAppToolbarview.backgroundColor = UIColor(red: 2.0/255, green: 97/255, blue: 153/255, alpha: 1)
        
        let gradient: CAGradientLayer = CAGradientLayer()
        //let colorTop =  UIColor(red: 2.0/255.0, green: 70.0/255.0, blue: 110.0/255.0, alpha: 1.0).cgColor

        let colorTop =  UIColor(red: 1.0/255.0, green: 50.0/255.0, blue: 80.0/255.0, alpha: 1.0).cgColor

        let colorBottom = UIColor(red: 2.0/255.0, green: 97.0/255.0, blue: 153.0/255.0, alpha: 1.0).cgColor
        gradient.colors = [colorTop,colorBottom]
        gradient.locations = [0.0 , 1.0]
        //gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        //gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.frame = CGRect(x: 0.0, y: 0.0, width: self.ShapeAppToolbarview.frame.size.width, height: self.ShapeAppToolbarview.frame.size.height)
//        self.ShapeAppToolbarview.layer.insertSublayer(gradient, at: 0)
        
        
        
        
        //---------*****Waiting List View Gradient colors-------*********//

        let WaitingListViewgradient: CAGradientLayer = CAGradientLayer()
        //let colorTop =  UIColor(red: 2.0/255.0, green: 70.0/255.0, blue: 110.0/255.0, alpha: 1.0).cgColor

        let WaitingListViewTop =  UIColor(red: 204/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.1).cgColor

        let WaitingListViewBottom = UIColor(red: 0/255.0, green: 51/255.0, blue: 102/255.0, alpha: 1.0).cgColor
        WaitingListViewgradient.colors = [WaitingListViewTop,WaitingListViewBottom]
        WaitingListViewgradient.locations = [0.0 , 1.0]
        //gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        //gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        WaitingListViewgradient.frame = CGRect(x: 0.0, y: 0.0, width: self.WaitingListUser.frame.size.width, height: self.WaitingListUser.frame.size.height)
//        self.WaitingListUser.layer.insertSublayer(WaitingListViewgradient, at: 0)


        //*********---------WaitingList User Textview Gradients-----------********//
        
        
        let WaitingListTextViewgradient: CAGradientLayer = CAGradientLayer()
        //let colorTop =  UIColor(red: 2.0/255.0, green: 70.0/255.0, blue: 110.0/255.0, alpha: 1.0).cgColor

        let WaitingListTextViewTop =  UIColor(red: 204/255.0, green: 255/255.0, blue: 255/255.0, alpha: 0.1).cgColor

        let WaitingListTextViewBottom = UIColor(red: 0/255.0, green: 51/255.0, blue: 102/255.0, alpha: 1.0).cgColor
        WaitingListTextViewgradient.colors = [WaitingListTextViewTop,WaitingListTextViewBottom]
        WaitingListTextViewgradient.locations = [0.0 , 1.0]
        //gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        //gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        WaitingListTextViewgradient.frame = CGRect(x: 0.0, y: 0.0, width: WaitingListUserTextview.frame.size.width, height: WaitingListUserTextview.frame.size.height)
//        self.WaitingListUserTextview.layer.insertSublayer(WaitingListTextViewgradient, at: 0)
        
        
        
        //*********-------CurrentUser Gradient color code---------*********//
        
        let CurrentUsergradient: CAGradientLayer = CAGradientLayer()
        //let colorTop =  UIColor(red: 2.0/255.0, green: 70.0/255.0, blue: 110.0/255.0, alpha: 1.0).cgColor
        
        let CurrentUserTop =  UIColor(red: 0/255.0, green: 255/255.0, blue: 205/255.0, alpha: 0.1).cgColor
        
        let CurrentUserBottom = UIColor(red: 102/255.0, green: 153/255.0, blue: 102/255.0, alpha: 1.0).cgColor
        CurrentUsergradient.colors = [CurrentUserTop,CurrentUserBottom]
        CurrentUsergradient.locations = [0.0 , 1.0]
        //gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        //gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        CurrentUsergradient.frame = CGRect(x: 0.0, y: 0.0, width: self.CurrentUserView.frame.size.width, height: self.CurrentUserView.frame.size.height)
//        self.CurrentUserView.layer.insertSublayer(CurrentUsergradient, at: 0)

        
        
        //********---------CurrentUser Textview Gradient color code----------******//
        
        let CurrentUsergradientTextview: CAGradientLayer = CAGradientLayer()
        //let colorTop =  UIColor(red: 2.0/255.0, green: 70.0/255.0, blue: 110.0/255.0, alpha: 1.0).cgColor
        
        let CurrentUserTopTextview =  UIColor(red: 0/255.0, green: 255/255.0, blue: 205/255.0, alpha: 0.1).cgColor
        
        let CurrentUserBottomTextview = UIColor(red: 102/255.0, green: 153/255.0, blue: 102/255.0, alpha: 1.0).cgColor
        CurrentUsergradientTextview.colors = [CurrentUserTopTextview,CurrentUserBottomTextview]
        CurrentUsergradientTextview.locations = [0.0 , 1.0]
        //gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        //gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        CurrentUsergradientTextview.frame = CGRect(x: 0.0, y: 0.0, width: self.CurrentUserTextView.frame.size.width, height: self.CurrentUserTextView.frame.size.height)
//        self.CurrentUserTextView.layer.insertSublayer(CurrentUsergradientTextview, at: 0)
        
        
        
        //---------*******Earned hours Gradient color code -------------**********//
        
        
        let Earnedhoursgradient: CAGradientLayer = CAGradientLayer()
        //let colorTop =  UIColor(red: 2.0/255.0, green: 70.0/255.0, blue: 110.0/255.0, alpha: 1.0).cgColor
        
        let EarnedhoursTop =  UIColor(red: 255/255.0, green: 153/255.0, blue: 255/255.0, alpha: 0.1).cgColor
        
        let EarnedhoursBottom = UIColor(red: 153/255.0, green: 0/255.0, blue: 204/255.0, alpha: 1.0).cgColor
        Earnedhoursgradient.colors = [EarnedhoursTop,EarnedhoursBottom]
        Earnedhoursgradient.locations = [0.0 , 1.0]
        //gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        //gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        Earnedhoursgradient.frame = CGRect(x: 0.0, y: 0.0, width: self.EarnedhoursView.frame.size.width, height: self.EarnedhoursView.frame.size.height)
//        self.EarnedhoursView.layer.insertSublayer(Earnedhoursgradient, at: 0)
        
        
        
        //**********-----Earnedhours Textview Gradient color code--------**********//
        
        let EarnedhoursgradientText: CAGradientLayer = CAGradientLayer()
        //let colorTop =  UIColor(red: 2.0/255.0, green: 70.0/255.0, blue: 110.0/255.0, alpha: 1.0).cgColor
        
        let EarnedhoursTopTextview =  UIColor(red: 255/255.0, green: 153/255.0, blue: 255/255.0, alpha: 0.1).cgColor
        
        let EarnedhoursBottomTextview = UIColor(red: 153/255.0, green: 0/255.0, blue: 204/255.0, alpha: 1.0).cgColor
        EarnedhoursgradientText.colors = [EarnedhoursTopTextview,EarnedhoursBottomTextview]
        EarnedhoursgradientText.locations = [0.0 , 1.0]
        //gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        //gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        EarnedhoursgradientText.frame = CGRect(x: 0.0, y: 0.0, width: self.EarnedhoursTextview.frame.size.width, height: self.EarnedhoursTextview.frame.size.height)
//        self.EarnedhoursTextview.layer.insertSublayer(EarnedhoursgradientText, at: 0)
        
        
        
        
        //--------Infoview Gradient color code------------//
        
        
        let GymInfogradient: CAGradientLayer = CAGradientLayer()
        //let colorTop =  UIColor(red: 2.0/255.0, green: 70.0/255.0, blue: 110.0/255.0, alpha: 1.0).cgColor
        
        let GymInfocolorTop =  UIColor(red: 255/255.0, green: 255/255.0, blue: 102/255.0, alpha: 0.1).cgColor
        
        
        //let GymInfocolorBottom =  UIColor(red: 255/255.0, green: 255/255.0, blue: 102/255.0, alpha: 0.1).cgColor

        
        let GymInfocolorBottom = UIColor(red: 255/255.0, green: 133/255.0, blue: 51/255.0, alpha: 1.0).cgColor
        GymInfogradient.colors = [GymInfocolorTop,GymInfocolorBottom]
        GymInfogradient.locations = [0.0 , 1.0]
        //gradient.startPoint = CGPoint(x: 0.0, y: 1.0)
        //gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        GymInfogradient.frame = CGRect(x: 0.0, y: 0.0, width: self.GymInfoview.frame.size.width, height: self.GymInfoview.frame.size.height)
//        self.GymInfoview.layer.insertSublayer(GymInfogradient, at: 0)
        
        
        
        
        
        
        
        
        
        
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("GYMDATA"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("GYM_HOURS"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(updategymdata), name: Notification.Name("GYMDATA"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updategymhours), name: Notification.Name("GYM_HOURS"), object: nil)

        updategymdata()
        updategymhours()
        QRCodeImgVw.image = qrCode
        QRCodeImgVw.layer.magnificationFilter = CALayerContentsFilter(rawValue: kCISamplerFilterNearest)
        
        
        
        
        
    }

    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    //-------------------------changes made by suresh-------------------//
    @IBAction func drpdwnbtn(_ sender: Any) {
        
        
        DropdownList()
        
        
    }
    
    
    //-------------------------changes made by suresh-------------------//
    func DropdownList()
    {
        
        if drpdwntableview.isHidden {
            drpdwntableview.isHidden = false


            //let tokenString = "Bearer " + Retrivedaccess_token






            //var Token = Retrivedaccess_token
            let dataURL = "http://shapeapp.net/api/gym/list-gyms"
            var request = URLRequest(url: URL(string: dataURL)!)

            //request.setValue(tokenString, forHTTPHeaderField: "Authorization")



            request.addValue("Bearer \("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjZmZjQ0OTg1ZTVhNTllZmIyNTcyZGU4Y2JlZDIyNmI3Y2RhMjk1N2YxMDI2YTk2ZGRjMWNlNmM4ZTUxODc0NmM4ZmI5NDFlNzU1ODYzM2EyIn0.eyJhdWQiOiIyIiwianRpIjoiNmZmNDQ5ODVlNWE1OWVmYjI1NzJkZThjYmVkMjI2YjdjZGEyOTU3ZjEwMjZhOTZkZGMxY2U2YzhlNTE4NzQ2YzhmYjk0MWU3NTU4NjMzYTIiLCJpYXQiOjE1NTUzMDYyMTQsIm5iZiI6MTU1NTMwNjIxNCwiZXhwIjoxNTg2OTI4NjE0LCJzdWIiOiI2MjMiLCJzY29wZXMiOlsiKiJdfQ.bviX2Kjnp6P33UsUqBCEisf2D29gTBg8qjr2B2X9dNpw6qK29brpQNI0l6xbGdQ7Cuysz_qGxJzuNm13khtFlq7S-N0vfNYtXIiegmg1xyn5cEDC5zzOpCajmuY6SAetFi3w-r6B0S74cWGKWzW1E7VR9nnRHAv1ij0OYl_ylkqloM5hzcMUyOJ5dOPXGuvx2VsP4P1ivCiXzq9hrlUxa9bgmfb52PS9DK36A7SzeJS9zk9CWvxdQFcb58Y-zKxsKqqXPlWlM-Y8OskzG2kzKsfxNU3oH4oZNj_nZOP2aN1n93CQ31nnI4HwDB7WcpQEdoaTCV8Qhw7N_NyJClpTsk5kb2F8ocnf5Ty11vkdqYCxh3b064QA3gf6dLN43fggwEny8pwgiXIKgMzp3mGk1aB4ZZ7TOQ-aEmw6PWBP58TnUeDB5OOCgUrak1LprbZTgPon4zTioYkN45XsQ_Pw40UoRBrfynQDK4V3iLcS2kDeXcjqIkNsqNfB8UccNu1d7NKtrf0KXVH1VPPlheLwOimiOQ8_qza4yrp2MWwjrY5SOoJCuEoCfQCrr6H3l1owEdcdWGxh8m0F_iQErqYXweBWmIsmCVo9VL8EddOXC0AEZ-KRG3EK3KhVj7boAWd1uBKDFnMPvjr3bjd0LbKWbQ8MixmhptiykLdOYzpVeF0")", forHTTPHeaderField: "Authorization")
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                // Check if the response has an error
                if error != nil{
                    print("Error \(String(describing: error))")
                    return
                }
                do {
                    let jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
                    print("jsonDictionary",jsonDictionary)
                    if let names = jsonDictionary["gym_name"] as? [String] {
                        print("Gymnames",names)
                    }
                    guard let jsonArray = jsonDictionary["data"] as? [[String: Any]] else {
                        return
                    }
                    print("Json Array",jsonArray)

                    for gymDic in jsonArray
                    {
                        let gymDictionary:NSMutableDictionary = NSMutableDictionary()

                        let gym_name = gymDic["gym_name"] as! String
                        print("gymname",gym_name)

                        //self.Multipleselectiondrpdwntxt.text = gym_name





                        let gym_address = gymDic["gym_address"] as! String
                        let  gym_location = gymDic["gym_location"] as! String
                        let gym_id = gymDic["gym_id"] as! Int

                        gymDictionary.setValue(gym_name, forKey: "gym_nameKey")
                        gymDictionary.setValue(gym_address, forKey: "gym_addressKey")
                        gymDictionary.setValue(gym_location, forKey: "gym_locationKey")
                        gymDictionary.setValue(gym_id, forKey: "gym_idKey")

                        self.gymArray.add(gymDictionary)
                    }
                    print(self.gymArray)
                    DispatchQueue.main.async {
                        self.drpdwntableview.reloadData()

                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
                if let httpResponse = response as? HTTPURLResponse{
                    if httpResponse.statusCode == 200{
                        print("Refresh token...")
                        return
                    }
                }
                }.resume()




        } else {
            drpdwntableview.isHidden = true
        }




        
        
    }
    
    
    
    
    
    @IBAction func refresh_data(_ sender: Any) {
        Appcontrol.refresh_gym_data()
        Appcontrol.refresh_onGoing_hours()
        Appcontrol.refresh_user_hours()
    }
    
    @objc func updategymdata(){
        
        let gymdata = Appcontrol.getgymdata()

        if let gymname = gymdata["gym_name"] {
           // GymName.setTitle(gymname as? String, for: UIControl.State.normal)
        }else {
            Appcontrol.refresh_gym_data()
        }
    }

    @objc func updategymhours(){
        
        let userhrs = UserDefaults.standard.dictionary(forKey: "GYMHOURS");
        
        if userhrs != nil {
            print(userhrs?.description ?? "")
            WaitingUserLbl.text = String(format: "%@", userhrs?["pending_users"] as! CVarArg)
            print("waitinglistUser",WaitingUserLbl.text)
            
            
            CurrentUserLbl.text = String(format: "%@", userhrs?["current_users"] as! CVarArg)
            
            print("Currentuserlist", CurrentUserLbl.text)
            EarnedHoursLbl.text = String(format: "%@ hrs", userhrs?["used_hours"] as! CVarArg)
            
            print("EarnedList",EarnedHoursLbl.text)
            
            
            
        }else {
            Appcontrol.refresh_user_hours()
            
            
            
            
        }
    }

    
    @IBAction func transactionhistory(_ sender: Any) {
        
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC: TransactionVC = storyBoard.instantiateViewController(withIdentifier:"TransactionVC") as! TransactionVC
        
        let navigationController: UINavigationController? = UINavigationController(rootViewController: rootVC)
        self.present(navigationController!, animated: true, completion: {
            
        })
        
    }

    @IBAction func currentUser(_ sender: Any) {
        
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC: Current_Pending_User_VC = storyBoard.instantiateViewController(withIdentifier:"Current_Pending_User_VC") as! Current_Pending_User_VC
        let navigationController: UINavigationController? = UINavigationController(rootViewController: rootVC)
        self.present(navigationController!, animated: true, completion: {
            
        })
        
    }
    
    
    
    @IBAction func Earnedhours(_ sender: Any) {
        
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC: Earnedhours_VC = storyBoard.instantiateViewController(withIdentifier:"Earnedhours_VC") as! Earnedhours_VC
        let navigationController: UINavigationController? = UINavigationController(rootViewController: rootVC)
        self.present(navigationController!, animated: true, completion: {
            
        })
        
    }
    
    

    
    @IBAction func gyminfo(_ sender: Any) {
        
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC: GymInfoViewController = storyBoard.instantiateViewController(withIdentifier:"GymInfoViewController") as! GymInfoViewController
        
        let navigationController: UINavigationController? = UINavigationController(rootViewController: rootVC)
        self.present(navigationController!, animated: true, completion: {
            
        })
        
    }

    
    
    
    
    
    
    
    
    
    
    private func createQRFromString(str: String) -> CIImage? {
        let stringData = str.data(using: .utf8)
        
        let filter = CIFilter(name: "CIQRCodeGenerator")
        filter?.setValue(stringData, forKey: "inputMessage")
        filter?.setValue("H", forKey: "inputCorrectionLevel")
        
        return filter?.outputImage
    }
    
    var qrCode: UIImage? {
        
        let gymdata = Appcontrol.getgymdata()
        if let gymname = gymdata["gym_ref"] {
            if let img = createQRFromString(str: gymname as! String) {
                
                let qrcodeImage = UIImage(
                    ciImage: img,
                    scale: 1.0,
                    orientation: UIImage.Orientation.down
                )
                return qrcodeImage
            }
        }

        
        
        return nil
    }
    
    
    //-------------------------changes made by suresh-------------------//
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gymArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:MultipleselectiondrpdwnCell = tableView.dequeueReusableCell(withIdentifier: "cellId") as! MultipleselectiondrpdwnCell
        let retrivedGymDic:NSDictionary = gymArray[indexPath.row] as! NSDictionary
       // let gymid:NSInteger = retrivedGymDic["gym_idKey"] as! NSInteger
        
        let dynamicgymid = retrivedGymDic["gym_idKey"] as! Int
        print("retrived dynamic gymid",dynamicgymid)

        
        
        
        let gymName:NSString = retrivedGymDic["gym_nameKey"] as! NSString
        let gym_address:NSString = retrivedGymDic["gym_addressKey"] as! NSString
        let gym_location:NSString = retrivedGymDic["gym_locationKey"] as! NSString
        cell.gymnamelbl.text = gymName as String
        
        
        
       
        
        gymidString = "\(dynamicgymid)"

        cell.idlbl.text = gymidString
        
        
        cell.idlbl.isHidden = true
        print("dynamic gymid value",cell.idlbl.text)

        
        
        
        //cell.gymAddress.text = gym_address as String
        //cell.city.text = gym_location as String
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let cell = tableView.cellForRow(at: indexPath) as! MultipleselectiondrpdwnCell
       // var level: String! = cell.gymnamelbl.text
        //var score: String! = cell.gymnamelbl.text
        
        Multipleselectiondrpdwntxt.text = cell.gymnamelbl.text
        
        //Anamicastr = cell.gymnamelbl.text!
        
        var idstr = cell.idlbl.text
        
        
        
        print("idvalue",idstr)
        
        
        //Updatehoursdata()
        
        
        
        
        var tokenstr = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijg0MmE4NThlYmFiYTE3ZWYwNzdjOTVjMzYxMjFmOTlmNDAzNjkxMmZjNWIzNDhjMjE1MWFmMWRhNTJkOWU2OWFiZTY2NTNlMjBjYjQzZTQwIn0.eyJhdWQiOiIyIiwianRpIjoiODQyYTg1OGViYWJhMTdlZjA3N2M5NWMzNjEyMWY5OWY0MDM2OTEyZmM1YjM0OGMyMTUxYWYxZGE1MmQ5ZTY5YWJlNjY1M2UyMGNiNDNlNDAiLCJpYXQiOjE1NTU0MDgxNjksIm5iZiI6MTU1NTQwODE2OSwiZXhwIjoxNTg3MDMwNTY5LCJzdWIiOiI2MjMiLCJzY29wZXMiOlsiKiJdfQ.Zj9E7CVIvAzxJpdmKh6Rj4O9JO2o_wy2_4eBWnnp89sxS3n5-7MmxKZIg2iMTLI1gzTvuLBqACfgP_fHIKy70QYJkly-P6qZKNoBKeJ58b6rOblzZJpridPfwpiTYF_qds2AnSLSw3HFJgWk-MCNBzABjY8jjtodAYQfDNYdercHLFYb32J19r1cBAVc2ctIS_S01_eX7-PFB2PnFuDafQ-hFkS9LRrC9hczomIrq_ADupwy6Fd2FTmIh6fa5IueS6KgKGvp0qPrd7rZABi0ywSyz18VmYRYHTjlFC_inisNgCw_M61lGNVwO8VQUyPisa2Jh6W5whV1CBAg_Yyb5qmJjqqJw5k_LwsmLWBZdXOdhN8Flfp0-a9QScwiAeMWIyFkKhQFMm8z_Frpgl8IVwpUs_ShqmmcDAyJZSxdOb59t2rtiEwFXljZXgfQiacSfwa0rlF2pewWslHXSR2wUmtnngzaUAARFzLIwVIA3UqU4YRWc5JDDkL0tw7EP0UPsclQlV_js-bsvgV6O8IG_5f-uYO-SE63oXRscOdxb6IxNE652IsCnste5hHI16rahrFpBnGrJNwBjKwU_cQG5n7yCS9-0Wa9lWL6pJycDlzwPSYYbxfuJ44cqhullQveC3O_7u3AzDhWp9Y8eXbCtmurN-zTuHaj5y7HImxJlvQ"
        
        let tokenString = "Bearer " + tokenstr
        let url = URL(string: "http://shapeapp.net/api/gym/hours-points")!
        let json: [String: Any] = ["gym_id": idstr]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            // Check if the response has an error
            if error != nil{
                print("Error \(String(describing: error))")
                return
            }
            do {
                let jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
                let dataDictionary:NSDictionary = jsonDictionary["data"] as! NSDictionary
                //let current_users = dataDictionary["current_users"] as! Int
                //let gym_points:NSString = dataDictionary["gym_points"] as! NSString
                //let ongoing_users:NSString = dataDictionary["ongoing_users"] as! NSString
                //let pending_users:NSString = dataDictionary["pending_users"] as! NSString
                
                
                
                let ongoing_users = dataDictionary["ongoing_users"] as! Int
                let pending_users = dataDictionary["pending_users"] as! Int
                
                
                
                let used_hours:NSString = dataDictionary["used_hours"] as! NSString
                
                
                
                
                
                
                
                print("current",ongoing_users)
                print("waiting",pending_users)
                print(used_hours)
                
                
                DispatchQueue.main.async {
                    
                    
                    var waitinglistString = ""
                    
                    waitinglistString = "\(pending_users)"
                    
                    
                    var currentuserlist = ""
                    currentuserlist = "\(ongoing_users)"
                    
                    
                    
                    self.WaitingUserLbl.text = waitinglistString
                    print("waiting list user",self.WaitingUserLbl.text)
                    
                    
                    self.CurrentUserLbl.text = currentuserlist
                    print("current user",self.CurrentUserLbl.text)
                    
                    
                    
                    
                    self.EarnedHoursLbl.text = used_hours as String
                    print("Earned hours",self.EarnedHoursLbl.text )
                    
                    
                    
                    
                    self.drpdwntableview.isHidden = true
                    
                    
                    
                }
                
                
                
                
                
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
            if let httpResponse = response as? HTTPURLResponse{
                if httpResponse.statusCode == 200{
                    print("Refresh token...")
                    return
                }
            }
            }.resume()
    }
    
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
       // drpdwntableview.isHidden = true
        
        //print("selected value",level)
        //print("selected ",score)
        
        
        
       
    }
    
    
    
    func Updatehoursdata()
    {
        
        let gym_id = "1"
        
        
//
//
//        var tokenstr = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijg0MmE4NThlYmFiYTE3ZWYwNzdjOTVjMzYxMjFmOTlmNDAzNjkxMmZjNWIzNDhjMjE1MWFmMWRhNTJkOWU2OWFiZTY2NTNlMjBjYjQzZTQwIn0.eyJhdWQiOiIyIiwianRpIjoiODQyYTg1OGViYWJhMTdlZjA3N2M5NWMzNjEyMWY5OWY0MDM2OTEyZmM1YjM0OGMyMTUxYWYxZGE1MmQ5ZTY5YWJlNjY1M2UyMGNiNDNlNDAiLCJpYXQiOjE1NTU0MDgxNjksIm5iZiI6MTU1NTQwODE2OSwiZXhwIjoxNTg3MDMwNTY5LCJzdWIiOiI2MjMiLCJzY29wZXMiOlsiKiJdfQ.Zj9E7CVIvAzxJpdmKh6Rj4O9JO2o_wy2_4eBWnnp89sxS3n5-7MmxKZIg2iMTLI1gzTvuLBqACfgP_fHIKy70QYJkly-P6qZKNoBKeJ58b6rOblzZJpridPfwpiTYF_qds2AnSLSw3HFJgWk-MCNBzABjY8jjtodAYQfDNYdercHLFYb32J19r1cBAVc2ctIS_S01_eX7-PFB2PnFuDafQ-hFkS9LRrC9hczomIrq_ADupwy6Fd2FTmIh6fa5IueS6KgKGvp0qPrd7rZABi0ywSyz18VmYRYHTjlFC_inisNgCw_M61lGNVwO8VQUyPisa2Jh6W5whV1CBAg_Yyb5qmJjqqJw5k_LwsmLWBZdXOdhN8Flfp0-a9QScwiAeMWIyFkKhQFMm8z_Frpgl8IVwpUs_ShqmmcDAyJZSxdOb59t2rtiEwFXljZXgfQiacSfwa0rlF2pewWslHXSR2wUmtnngzaUAARFzLIwVIA3UqU4YRWc5JDDkL0tw7EP0UPsclQlV_js-bsvgV6O8IG_5f-uYO-SE63oXRscOdxb6IxNE652IsCnste5hHI16rahrFpBnGrJNwBjKwU_cQG5n7yCS9-0Wa9lWL6pJycDlzwPSYYbxfuJ44cqhullQveC3O_7u3AzDhWp9Y8eXbCtmurN-zTuHaj5y7HImxJlvQ"
//
//        let tokenString = "Bearer " + tokenstr
//        let url = URL(string: "http://shapeapp.net/api/gym/hours-points")!
//        let json: [String: Any] = ["gym_id": idstr]
//        let jsonData = try? JSONSerialization.data(withJSONObject: json)
//        var request = URLRequest(url: url)
//        request.httpMethod = "POST"
//        request.httpBody = jsonData
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
//        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
//        URLSession.shared.dataTask(with: request) { (data, response, error) in
//            // Check if the response has an error
//            if error != nil{
//                print("Error \(String(describing: error))")
//                return
//            }
//            do {
//                let jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
//                let dataDictionary:NSDictionary = jsonDictionary["data"] as! NSDictionary
//                let current_users = dataDictionary["current_users"] as! Int
//                let gym_points:NSString = dataDictionary["gym_points"] as! NSString
//                //let ongoing_users:NSString = dataDictionary["ongoing_users"] as! NSString
//                //let pending_users:NSString = dataDictionary["pending_users"] as! NSString
//
//
//
//                //let ongoing_users = dataDictionary["ongoing_users"] as! Int
//                //let pending_users = dataDictionary["pending_users"] as! Int
//
//
//
//                let used_hours:NSString = dataDictionary["used_hours"] as! NSString
//
//                print("current users",current_users)
//                print("gympoints",gym_points)
//
//
//                //WaitingUserLbl.text = used_hours as String
////                self.WaitingUserLbl.text = pending_users as String
////                print("waiting list users lable",self.WaitingUserLbl.text)
////
//                //self.CurrentUserLbl.text = ongoing_users as String
//                //print("current list users",self.CurrentUserLbl.text)
//
////                self.EarnedHoursLbl.text = used_hours as String
////                print("Earned hours",self.EarnedHoursLbl.text )
//
//
//
//
//
//                //print(ongoing_users)
//                //print(pending_users)
//                print(used_hours)
//
//
//                DispatchQueue.main.async {
//
//                    self.EarnedHoursLbl.text = used_hours as String
//                    print("Earned hours",self.EarnedHoursLbl.text )
//
//
//
//                }
//
//
//
//
//
//            } catch let error as NSError {
//                print("Failed to load: \(error.localizedDescription)")
//            }
//            if let httpResponse = response as? HTTPURLResponse{
//                if httpResponse.statusCode == 200{
//                    print("Refresh token...")
//                    return
//                }
//            }
//            }.resume()
//    }
//
}
    


