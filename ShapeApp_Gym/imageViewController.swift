//
//  imageViewController.swift
//  ShapeApp
//
//  Created by Satheesh Kumar on 09/03/19.
//  Copyright © 2019 UniqueAngels. All rights reserved.
//

import UIKit

class imageViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate {

    var imagelist: NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false;
    }
    
    

}


extension imageViewController {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imagelist.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let CVC:ImageGalleryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageGalleryCell", for: indexPath) as! ImageGalleryCell
        let url = imagelist.object(at: indexPath.row) as! String
        CVC.imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "NoImage"), options:SDWebImageOptions.refreshCached)
        CVC.contentView.backgroundColor = #colorLiteral(red: 0.3098039329, green: 0.01568627544, blue: 0.1294117719, alpha: 1)
        return CVC;
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.size.width-150, height: 1000)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        //        let rootVC: GymInfoViewController = storyBoard.instantiateViewController(withIdentifier:"GymInfoViewController") as! GymInfoViewController
        //        rootVC.gymData = gymlist.object(at: indexPath.row) as! NSDictionary
        //        self.navigationController?.pushViewController(rootVC, animated: true)
        
        
    }
}
