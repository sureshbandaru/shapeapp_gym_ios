//
//  Appcontrol.m
//  ShapeApp
//
//  Created by Satheesh Kumar on 12/11/18.
//  Copyright © 2018 UniqueAngels. All rights reserved.
//

#import "Appcontrol.h"
#import <FirebaseInstanceID/FirebaseInstanceID.h>

static Appcontrol *_sharedManager;


@implementation Appcontrol
@synthesize loading;

+ (Appcontrol *)sharedManager
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[Appcontrol alloc] init];
        
    });
    return _sharedManager;
}

#pragma AUTHORAIZATION

+ (BOOL)api_authorization:(NSString *)code password:(NSString *)number type:(int)type{
    
    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:code,@"country_code",number,@"mobile", nil];
    NSData *requestData = [NSData dataWithBytes: [dict.JSONRepresentation UTF8String] length: [dict.JSONRepresentation length]];

    NSMutableURLRequest   *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"%s/user/check",OAUTH_URL]]];
    [request setHTTPMethod: @"POST"];
    [request setTimeoutInterval:45];
    NSString *contenlength = [NSString stringWithFormat:@"%d",(int)[requestData length]];
    [request setValue:@" application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:contenlength forHTTPHeaderField:@"Content-Length"];
    
    [request setHTTPBody:requestData];

    NSData *data;
    NSURLResponse *response;
    NSError *error;
    
    // Make synchronous request
    data = [NSURLConnection sendSynchronousRequest:request
                                 returningResponse:&response
                                             error:&error];
    NSString *returnString;
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
    
    if ([httpResponse statusCode]==200) {
        if (data !=nil) {
            returnString = [[NSString alloc] initWithData:data encoding: NSUTF8StringEncoding];
            //        returnarray =[returnString JSONValue];
            NSLog(@"returnstr--%@",returnString);
            
            NSDictionary *dict = [returnString JSONValue];
            NSLog(@"%@",dict.description);
            
            if ( dict ) {
                
                [default_prefrence setObject:[returnString JSONValue] forKey:@"API_TOKEN"];
                [default_prefrence setObject:[NSDate date] forKey:@"TOKEN_REG_TIME"];
                [default_prefrence synchronize];
                return YES;
            }else{
                NSLog(@"invalid");
                return NO;
                
            }
            //        NSString *trimmedString = [returnString stringByTrimmingCharactersInSet: [NSCharacterSet newlineCharacterSet]];
            
            
        }else{
            
            NSLog(@"ERROR IN CONNECTION");
            return NO;
            
        }
        
    }else if ([httpResponse statusCode]==0){
        if (data !=nil) {
            returnString = [[NSString alloc] initWithData:data encoding: NSUTF8StringEncoding];
            //        returnarray =[returnString JSONValue];
            NSLog(@"returnstr--%@",returnString);
            
            if ([returnString JSONValue]) {
                
                NSDictionary*dict=[returnString JSONValue];
                NSLog(@"%@",dict.description);
//                [Milan_Manager through_notify_mess:dict[@"error_description"]];
                
            }
        } else {
//            [Milan_Manager through_notify_mess:@"Internet connection appears to be offline"];
        }
        
        return NO;
    }else{
        
//        [Milan_Manager through_notify_mess:@"Please try again"];
        
        return NO;
    }
    
}

+ (NSString *)getAccessToken {
    
    NSDictionary *dict =     [default_prefrence valueForKey:@"API_TOKEN"];
    

    
    

    return [NSString stringWithFormat:@"%@ %@",dict[@"token_type"],dict[@"access_token"]];
}

+ (NSDictionary *)getuserdata {
    
    NSData *data = [default_prefrence objectForKey:@"USERDICT"];
    NSDictionary *dict = (NSDictionary*)[NSKeyedUnarchiver unarchiveObjectWithData:data];

    return dict;
}

+ (NSDictionary *)getgymdata {
    
    NSData *data = [default_prefrence objectForKey:@"GYMDICT"];
    NSDictionary *dict = (NSDictionary*)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    return dict;
}


+ (void)shakeView :(id)object{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIView *view = (UIView *)object;
        
        CABasicAnimation *shake = [CABasicAnimation animationWithKeyPath:@"position"];
        [shake setDuration:0.07];
        [shake setRepeatCount:2];
        [shake setAutoreverses:YES];
        [shake setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake(view.center.x - 5,view.center.y)]];
        [shake setToValue:[NSValue valueWithCGPoint:
                           CGPointMake(view.center.x + 5, view.center.y)]];
        [view.layer addAnimation:shake forKey:@"position"];
        
    });
    
}


+ (NSString *)common_http_request:(NSString *)path value:(NSString *)values type:(int)type isjson:(bool)json loader:(bool)loader{
    
    if (loader) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[Appcontrol sharedManager] startloading];
        });
    }
    
    NSDictionary *diction=[default_prefrence valueForKey:@"API_TOKEN"];
    
    NSString *urll;
    urll = [NSString stringWithFormat:@"%s%@",OAUTH_URL,path];
    //    NSLog(@"==%@",urll);
    NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urll]];
    
    if (type==GET_METHOD) {
        
        if(![values isKindOfClass:[NSDictionary class]]){
            if ([values length] > 0) {
                urll=[NSString stringWithFormat:@"%@?%@",urll,values];
                //                urll=[NSString stringWithFormat:@"%@?",urll];
                
            }
        }
        
        [req setURL:[NSURL URLWithString:[[urll componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@""]]];
        [req setHTTPMethod: @"GET"];
        
    }else if (type==POST_METHOD){
        
        NSData *datasend = [values dataUsingEncoding:NSUTF8StringEncoding];
        [req setHTTPMethod:@"POST"];
        [req setHTTPBody:datasend];
        
    }else if(type==DELETE_METHOD){
        
        NSData *datasend = [values dataUsingEncoding:NSUTF8StringEncoding];
        [req setHTTPMethod:@"DELETE"];
        [req setHTTPBody:datasend];
    }
    
    [req setValue:[NSString stringWithFormat:@"%@ %@",diction[@"token_type"],diction[@"access_token"]] forHTTPHeaderField:@"Authorization"];
    [req setValue:X_API_KEY forHTTPHeaderField:@"X-API-Key"];
    
    
    if (json) {
        [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    }
    [req setTimeoutInterval:45];
    //    NSURLResponse *response;
    NSError *error=nil;
    NSString *returnString;
    NSLog(@"%@--%@",urll,values);
    NSLog(@"%@--%@",diction[@"token_type"],diction[@"access_token"]);
    
    //    NSLog(@"Req header: %@",req.allHTTPHeaderFields.description);
    //    NSData *data= [NSURLConnection sendSynchronousRequest:req returningResponse:&response error:&error];
    //    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
    
    __block NSData *data = nil;
    __block NSHTTPURLResponse *httpResponse = nil;
    
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:req completionHandler:^(NSData *taskData, NSURLResponse *response, NSError *error) {
        data = taskData;
        httpResponse = (NSHTTPURLResponse *) response;
        if (!data) {
            NSLog(@"dispatch_semaphore_t = %@", error.localizedDescription);
        }
        dispatch_semaphore_signal(semaphore);
    }];
    [dataTask resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    
    //    NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
    //    NSLog(@"description: %@", httpResponse.description);
    //    NSLog(@"%@",error.localizedDescription);
    //    NSLog(@"response error code: %ld",(long)error.code);
    
    if ([httpResponse statusCode] != 401 ) {
        if (data !=nil) {
            returnString = [[NSString alloc] initWithData:data encoding: NSUTF8StringEncoding];
            
            NSLog(@"returnstr--%@",returnString);
            if (loader) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[Appcontrol sharedManager] stoploading];
                });
            }
            
            if ([returnString JSONValue]) {
                
                return returnString;
                
            }else{
                NSLog(@"invalid");
                return returnString;
                
            }
            //        NSString *trimmedString = [returnString stringByTrimmingCharactersInSet: [NSCharacterSet newlineCharacterSet]];
            
        }else{
            
            NSLog(@"ERROR IN CONNECTION");
            if (loader) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[Appcontrol sharedManager] stoploading];
                });
            }
            return nil;
        }
        
    } else if ( [httpResponse statusCode] == 401 || error.code == -1012 ){
        
        NSLog(@"UNAUTHORIZED");
    }
    
    if (loader) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[Appcontrol sharedManager] stoploading];
        });
    }
    
    return nil;
}

+ (void) logout{


    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                                           NSError * _Nullable error) {
        
        NSString * token = @"";
        if (error != nil) {
            NSLog(@"Error fetching remote instance ID: %@", error);
        } else {
            NSLog(@"Remote instance ID token: %@", result.token);
            token = result.token;
        }
        
        NSDictionary *respdict = [[Appcontrol common_http_request:@"/logout" value:[NSString stringWithFormat:@"{\"token\":\"%@\"}",token] type:POST_METHOD isjson:true loader:true] JSONValue];
        
        if (respdict) {
            NSLog(@"%@",respdict.description);
            if ( [respdict[@"status"] integerValue] == 200 ) {
                [Appcontrol logout_final];
            }
        } else {
            [Appcontrol logout_final];
        }

        
    }];
    

}

+ (void) logout_final {

    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [default_prefrence removePersistentDomainForName:appDomain];
    [default_prefrence setBool:false forKey:@"LOGGEDIN"];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        [UIApplication sharedApplication].keyWindow.rootViewController = [storyboard instantiateInitialViewController];
    });
    
}

+ (void) refresh_user_data{
    
    NSDictionary *respdict = [[Appcontrol common_http_request:@"/user" value:@"" type:GET_METHOD isjson:true  loader:false] JSONValue];
    
    
    NSLog(@"suresh :: %@", respdict);

    
    
    
    
    if (respdict) {
        NSLog(@"%@",respdict.description);
        
        NSData *dictdata = [NSKeyedArchiver archivedDataWithRootObject:respdict requiringSecureCoding:false error:nil];
        [default_prefrence setObject:dictdata forKey:@"USERDICT"];
    }
}

+ (void) refresh_gym_data{
    
    NSDictionary *respdict = [[Appcontrol common_http_request:@"/gym-data" value:@"" type:GET_METHOD isjson:true loader:false] JSONValue];
    
    if (respdict) {
        NSLog(@"%@",respdict.description);
        NSArray *arr = respdict[@"data"];
        if (arr.count > 0) {
            NSData *dictdata = [NSKeyedArchiver archivedDataWithRootObject:arr[0] requiringSecureCoding:false error:nil];
            
            [default_prefrence setObject:dictdata forKey:@"GYMDICT"];
            
            
            
            
            NSString *savedValue = [[NSUserDefaults standardUserDefaults]
                                    stringForKey:@"preferenceName"];

            
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GYMDATA" object:self userInfo:nil];
            });

        }
    }
}



+ (void) refresh_user_hours{
    
    NSDictionary *respdict = [[Appcontrol common_http_request:@"/gym/hours-points" value:@"" type:POST_METHOD isjson:true loader:false] JSONValue];
    
    if (respdict) {
        NSLog(@"%@",respdict.description);
        if ( [respdict[@"status"] integerValue] == 200 ) {
            
            
            
            
            [default_prefrence setValue:[NSDate date] forKey:@"REFRESH_DATE"];
            [default_prefrence setObject:respdict[@"data"] forKey:@"GYMHOURS"];
            [default_prefrence synchronize];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"GYM_HOURS" object:self userInfo:nil];
            });

        }
    }
}

+ (void) refresh_onGoing_hours{
    
    NSDictionary *respdict = [[Appcontrol common_http_request:@"/user/ongoing" value:@"" type:GET_METHOD isjson:false loader:false] JSONValue];
    
    if (respdict) {
        NSLog(@"%@",respdict.description);
        if ( [respdict[@"status"] integerValue] == 200 ) {
            if ([[respdict[@"data"][@"message"] lowercaseString] isEqualToString:@"currently in gym"]) {
                NSData *dictdata = [NSKeyedArchiver archivedDataWithRootObject:respdict[@"data"] requiringSecureCoding:false error:nil];
                [default_prefrence setObject:dictdata forKey:@"ONGOING"];
            } else {
                [default_prefrence setObject:nil forKey:@"ONGOING"];
            }
            [default_prefrence synchronize];
        }else {
            [default_prefrence setObject:nil forKey:@"ONGOING"];
        }
    }else {
        [default_prefrence setObject:nil forKey:@"ONGOING"];
    }
    
    usleep(500);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ONGOING" object:self userInfo:nil];

}


+ (void) get_locality_name{
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"LAT"]) {
        
        NSMutableURLRequest   *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?latlng=%@,%@&key=%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"LAT"],[[NSUserDefaults standardUserDefaults] valueForKey:@"LON"],GOOGLE_API_KEY]]];
        [request setHTTPMethod: @"POST"];
        [request setTimeoutInterval:45];
        NSData *data;
        NSURLResponse *response;
        NSError *error;
        
        // Make synchronous request
        data = [NSURLConnection sendSynchronousRequest:request
                                     returningResponse:&response
                                                 error:&error];
        NSString *returnString;
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
        
        if ([httpResponse statusCode]==200) {
            if (data !=nil) {
                returnString = [[NSString alloc] initWithData:data encoding: NSUTF8StringEncoding];
                //        returnarray =[returnString JSONValue];
//                NSLog(@"returnstr--%@",returnString);
                
                NSDictionary *dict = [returnString JSONValue];
                
                if ( dict ) {
                    
                    [default_prefrence setObject:[returnString JSONValue] forKey:@"API_TOKEN"];
                    [default_prefrence setObject:[NSDate date] forKey:@"TOKEN_REG_TIME"];
                    [default_prefrence synchronize];
                }else{
                    NSLog(@"invalid");
                    
                }
                //        NSString *trimmedString = [returnString stringByTrimmingCharactersInSet: [NSCharacterSet newlineCharacterSet]];
                
                
            }else{
                
                NSLog(@"ERROR IN CONNECTION");
                
            }
            
        }else if ([httpResponse statusCode]==0){
            if (data !=nil) {
                returnString = [[NSString alloc] initWithData:data encoding: NSUTF8StringEncoding];
                //        returnarray =[returnString JSONValue];
//                NSLog(@"returnstr--%@",returnString);
                
                if ([returnString JSONValue]) {
                    
                    NSDictionary*dict=[returnString JSONValue];
                    //                [Milan_Manager through_notify_mess:dict[@"error_description"]];
                    
                }
            } else {
                //            [Milan_Manager through_notify_mess:@"Internet connection appears to be offline"];
            }
            
        }

    }
}

- (void) startloading{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self->loading removeFromSuperview];
        [self->_loading_overlay removeFromSuperview];
        
        self->loading=nil;
        self->_loading_overlay = nil;
        UIWindow* mainWindow = [[UIApplication sharedApplication] keyWindow];
        
        //        UIWindow* mainWindow = [Milan_Manager getTopMostViewController].view.window;
        
        self->_loading_overlay =[[UIView alloc]init];
        self->_loading_overlay.center = mainWindow.center;
        self->_loading_overlay.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        self->_loading_overlay.translatesAutoresizingMaskIntoConstraints = false;
        [mainWindow addSubview: self->_loading_overlay];
        
        self->loading =[[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
        self->loading.center=mainWindow.center;
        [mainWindow addSubview: self->loading];
        [self->loading startAnimating];
        
        NSDictionary *views = NSDictionaryOfVariableBindings(_loading_overlay);
        [mainWindow addConstraints:
         [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[_loading_overlay]-0-|" options:0 metrics:nil views:views]];
        [mainWindow addConstraints:
         [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_loading_overlay]-0-|" options:0 metrics:nil views:views]];
        [Appcontrol beginIgnoringInteractionEvents];

    });
    
}

- (void) stoploading{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self->loading stopAnimating];
        [self->loading removeFromSuperview];
        [self->_loading_overlay removeFromSuperview];
        self->_loading_overlay=nil;
        self->loading=nil;
        [Appcontrol endIgnoringInteractionEvents];
        
    });
    
}

+ (NSInteger)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate inSec:(BOOL) inSec
{
    NSDate* enddate = checkEndDate;
    NSDate* currentdate = [NSDate date];
    NSTimeInterval distanceBetweenDates = [enddate timeIntervalSinceDate:currentdate];
    
    if (inSec) {
        return fabs(distanceBetweenDates);
    }
    double minInHour = 60;
    NSInteger miniutesBetweenDates = distanceBetweenDates / minInHour;
    
    NSLog(@"%@ - %@",checkEndDate,[NSDate date]);
    NSLog(@"miniutesBetweenDates = %ld",(long)miniutesBetweenDates);
    return labs(miniutesBetweenDates);
    //    if (secondsBetweenDates == 0)
    //        return YES;
    //    else if (secondsBetweenDates < 0)
    //        return YES;
    //    else
    //        return NO;
}


+ (void) beginIgnoringInteractionEvents{
    if (![[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
        [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
    }
}

+ (void) endIgnoringInteractionEvents{
    if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
}


@end
