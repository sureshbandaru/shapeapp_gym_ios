//
//  Appcontrol.h
//  ShapeApp
//
//  Created by Satheesh Kumar on 12/11/18.
//  Copyright © 2018 UniqueAngels. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Appcontrol : NSObject

@property(nonatomic,strong) UIActivityIndicatorView *loading;
@property(nonatomic,strong) UIView *loading_overlay;

+ (Appcontrol *) sharedManager;

+ (BOOL)api_authorization:(NSString *)code password:(NSString *)number type:(int)type;
+ (NSString *)common_http_request:(NSString *)path value:(NSString *)values type:(int)type isjson:(bool)json loader:(bool)loader;
+ (void) refresh_user_data;
+ (void) refresh_gym_data;
+ (void) refresh_user_hours;
+ (void) get_locality_name;
+ (void) refresh_onGoing_hours;
+ (NSInteger)isEndDateIsSmallerThanCurrent:(NSDate *)checkEndDate inSec:(BOOL) inSec;

+ (NSDictionary *)getuserdata;
+ (NSDictionary *)getgymdata;
+ (void) logout;
+ (NSString *)getAccessToken;


+ (void)shakeView :(id)object;

- (void) startloading;
- (void) stoploading;
@end

NS_ASSUME_NONNULL_END
