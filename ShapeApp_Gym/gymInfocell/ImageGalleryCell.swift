//
//  ImageGalleryCell.swift
//  ShapeApp
//
//  Created by Satheesh Kumar on 09/03/19.
//  Copyright © 2019 UniqueAngels. All rights reserved.
//

import UIKit

class ImageGalleryCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
}
