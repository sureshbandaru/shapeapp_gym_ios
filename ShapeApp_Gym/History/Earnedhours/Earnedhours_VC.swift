//
//  Earnedhours_VC.swift
//  ShapeApp_Gym
//
//  Created by mac on 18/04/19.
//  Copyright © 2019 Unique Angle. All rights reserved.
//

import UIKit

class Earnedhours_VC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    var gymhoursArray:NSMutableArray = NSMutableArray()

    
    @IBOutlet weak var Earnedhrs_tblTV: UITableView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Earnedhours History"
        let barButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(dismissVC))
        barButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = barButtonItem;
        
        
        
        
        self.Earnedhrs_tblTV.register((UINib (nibName: "EarnedhoursCell", bundle: nil)), forCellReuseIdentifier: "cellId")
        
        
        
        let gym_id = "1"
        
        
        var tokenstr = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijg0MmE4NThlYmFiYTE3ZWYwNzdjOTVjMzYxMjFmOTlmNDAzNjkxMmZjNWIzNDhjMjE1MWFmMWRhNTJkOWU2OWFiZTY2NTNlMjBjYjQzZTQwIn0.eyJhdWQiOiIyIiwianRpIjoiODQyYTg1OGViYWJhMTdlZjA3N2M5NWMzNjEyMWY5OWY0MDM2OTEyZmM1YjM0OGMyMTUxYWYxZGE1MmQ5ZTY5YWJlNjY1M2UyMGNiNDNlNDAiLCJpYXQiOjE1NTU0MDgxNjksIm5iZiI6MTU1NTQwODE2OSwiZXhwIjoxNTg3MDMwNTY5LCJzdWIiOiI2MjMiLCJzY29wZXMiOlsiKiJdfQ.Zj9E7CVIvAzxJpdmKh6Rj4O9JO2o_wy2_4eBWnnp89sxS3n5-7MmxKZIg2iMTLI1gzTvuLBqACfgP_fHIKy70QYJkly-P6qZKNoBKeJ58b6rOblzZJpridPfwpiTYF_qds2AnSLSw3HFJgWk-MCNBzABjY8jjtodAYQfDNYdercHLFYb32J19r1cBAVc2ctIS_S01_eX7-PFB2PnFuDafQ-hFkS9LRrC9hczomIrq_ADupwy6Fd2FTmIh6fa5IueS6KgKGvp0qPrd7rZABi0ywSyz18VmYRYHTjlFC_inisNgCw_M61lGNVwO8VQUyPisa2Jh6W5whV1CBAg_Yyb5qmJjqqJw5k_LwsmLWBZdXOdhN8Flfp0-a9QScwiAeMWIyFkKhQFMm8z_Frpgl8IVwpUs_ShqmmcDAyJZSxdOb59t2rtiEwFXljZXgfQiacSfwa0rlF2pewWslHXSR2wUmtnngzaUAARFzLIwVIA3UqU4YRWc5JDDkL0tw7EP0UPsclQlV_js-bsvgV6O8IG_5f-uYO-SE63oXRscOdxb6IxNE652IsCnste5hHI16rahrFpBnGrJNwBjKwU_cQG5n7yCS9-0Wa9lWL6pJycDlzwPSYYbxfuJ44cqhullQveC3O_7u3AzDhWp9Y8eXbCtmurN-zTuHaj5y7HImxJlvQ"
        
        let tokenString = "Bearer " + tokenstr
        
        
        
        //let token = "here is the token"
        let url = URL(string: "http://shapeapp.net/api/gym/earned-hours")!
        
        // prepare json data
        let json: [String: Any] = ["gym_id": gym_id]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        
        // create post request
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        request.setValue(tokenString, forHTTPHeaderField: "Authorization")
        
        //            let parameters: [String: Any] = ["gym_id": "1"]
        //
        //            request.httpBody = try! JSONSerialization.data(withJSONObject: parameters )
        
        
        
        
        
        
        
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            // Check if the response has an error
            if error != nil{
                print("Error \(String(describing: error))")
                return
            }
            do {
                let jsonDictionary = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
                print(" suresh jsonDictionary",jsonDictionary)
                
                
                
                if let documentTone = jsonDictionary["data"] as? [String:Any],
                    let toneCategories = documentTone["gym_points"] as? [[String:Any]] {
                    for category in toneCategories {
                        print(category["gym_points"])
                    }
                }
                
                
                
                
                
                
                
                
                
                
                if let names = jsonDictionary["gym_name"] as? [String] {
                    print("Gymnames",names)
                }
                guard let jsonArray = jsonDictionary["data"] as? [[String: Any]] else {
                    return
                }
                print("Json Array",jsonArray)
                
                
                
                
                
                
                
                for gymDic in jsonArray
                {
                    let gymDictionary:NSMutableDictionary = NSMutableDictionary()
                    
                    let date = gymDic["date"] as! String
                    
//
                    
                    print("date",date)
                    //print("points",gym_points)
                    
                    
                    
                    
                    gymDictionary.setValue(date, forKey: "datekey")
                    //gymDictionary.setValue(gym_points, forKey: "gympointssKey")
                    //gymDictionary.setValue(gym_location, forKey: "gym_locationKey")
                    //gymDictionary.setValue(gym_id, forKey: "gym_idKey")
                    
                    self.gymhoursArray.add(gymDictionary)
                }
                print("suresh",self.gymhoursArray)
                
                DispatchQueue.main.async {
                    
                    self.Earnedhrs_tblTV.reloadData()

                    
                    
                    
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
            if let httpResponse = response as? HTTPURLResponse{
                if httpResponse.statusCode == 200{
                    print("Refresh token...")
                    return
                }
            }
            }.resume()
        
    }
    
    
    
    
        
        
        
        
        

        // Do any additional setup after loading the view.
    
    

    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.gymhoursArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:EarnedhoursCell = tableView.dequeueReusableCell(withIdentifier: "cellId") as! EarnedhoursCell
        let retrivedGymDic:NSDictionary = gymhoursArray[indexPath.row] as! NSDictionary
        let date:NSString = retrivedGymDic["datekey"] as! NSString
        //let gym_address:NSString = retrivedGymDic["gym_addressKey"] as! NSString
        //let gym_location:NSString = retrivedGymDic["gym_locationKey"] as! NSString
        cell.datelbl.text = date as String
    
        
        return cell
    }
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
