//
//  CP_users_Cell.swift
//  ShapeApp_Gym
//
//  Created by Satheesh Kumar on 17/03/19.
//  Copyright © 2019 Unique Angle. All rights reserved.
//

import UIKit

class CP_users_Cell: UITableViewCell {

    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var timeleftLbl: UILabel!
    
    @IBOutlet weak var usrImgLbl: UIImageView!
    @IBOutlet weak var HrsLble: UILabel!
    @IBOutlet weak var StimeLbl: UILabel!
    @IBOutlet weak var eTimeLbl: UILabel!
    @IBOutlet weak var StatusLbl: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
