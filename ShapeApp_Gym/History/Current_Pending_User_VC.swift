//
//  Current_Pending_User_VC.swift
//  ShapeApp_Gym
//
//  Created by Satheesh Kumar on 17/03/19.
//  Copyright © 2019 Unique Angle. All rights reserved.
//

import UIKit

class Current_Pending_User_VC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    var Transactionlist:NSArray = [];
    var FilteredTransactionlist:NSArray = [];
    @IBOutlet weak var TableView: UITableView!
    let dateFormatterGet = DateFormatter()
    let dateFormatterPrint = DateFormatter()
    @IBOutlet weak var noTransactionLbl: UILabel!
    @IBOutlet weak var segmentCtrl: UISegmentedControl!
    var Cdate: NSDate?;
    var refreshctrl: UIRefreshControl!

    override func viewDidLoad() {
        
        super.viewDidLoad()

        dateFormatterGet.dateFormat = "HH:mm:ss"
        dateFormatterPrint.dateFormat = "hh:mm a"
        Cdate = dateFormatterGet.date(from: dateFormatterGet.string(from: Date()))! as NSDate

        self.title = "Current User"
        let barButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(dismissVC))
        barButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = barButtonItem;

        refreshctrl = UIRefreshControl ()
        refreshctrl.attributedTitle = NSAttributedString(string: "Pull to refresh",attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)])
        refreshctrl.addTarget(self, action: #selector(getUsersList), for: UIControl.Event.valueChanged)
        
        TableView.addSubview(refreshctrl)
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getUsersList()
    }
    
    @IBAction func segmentSelected(_ sender: Any) {
        
        
        filterArray()
    }
    
    
    
    //---------------------------changes made bu suresh----------------------//
    @objc func getUsersList() {
        
        let respData = Appcontrol.common_http_request("/gym/current-users", value: "{\"gym_id\":\"\("1"))\"}", type:POST_METHOD, isjson: true, loader: true)
        
        
        
        
        //let respData = Appcontrol.common_http_request("/gym/history", value: "{\"start_date\":\"\(from)\",\"end_date\":\"\(to)\",\"gym_id\":\"\("1"))\"}", type:POST_METHOD, isjson: true, loader: true)
        
        
        
        
        
        
        
        if respData.jsonValue() != nil {
            if let userDetailsDictionary: NSDictionary = respData.jsonValue()! as? NSDictionary{
                
                print("current user data",userDetailsDictionary)
                
                
                
                if let code:Int = userDetailsDictionary.value(forKey: "status") as? Int {
                    if code == 200 {
                        Transactionlist = userDetailsDictionary.value(forKey: "data") as! NSArray
                        filterArray()
                    }
                }
            }
            
        }
        
        refreshctrl.endRefreshing()
    }

    func filterArray() {
        
        var status:String = "";
        if ( segmentCtrl.selectedSegmentIndex == 0 ) {
            self.title = "Waiting List"
            status = "Pending"
        } else {
            status = "Ongoing"
            self.title = "Current Users"
        }

        let resultPredicate = NSPredicate(format: "(status CONTAINS[c] '\(status)')")
        FilteredTransactionlist = Transactionlist.filtered(using: resultPredicate) as NSArray
        
        TableView.reloadData()
        
        if FilteredTransactionlist.count > 0 {
            noTransactionLbl.isHidden = true
            //                            TransactionTable.isHidden = false
        } else {
            //                            TransactionTable.isHidden = true
            noTransactionLbl.isHidden = false
            noTransactionLbl.text = "No Transaction Found"
            
        }

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FilteredTransactionlist.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:CP_users_Cell = self.TableView.dequeueReusableCell(withIdentifier: "CP_users_Cell") as! CP_users_Cell
        
        let dict:NSDictionary = FilteredTransactionlist.object(at: indexPath.row) as! NSDictionary
        cell.usernameLbl.text = dict["name"] as? String
        cell.HrsLble.text = "\(dict["Hours"] as? String ?? "0")hr"
        
        
        //-------------------------changes made by suresh-----------------------//
        
        
        
        var imgstr = dict["user_image"] as? String
        print("image str",imgstr)
        
//        cell.usrImgLbl.image = [UIImage, imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url_Img_FULL]]];

        let url = URL(string: imgstr!)
        let data = try? Data(contentsOf: url!)
        
        if let imageData = data {
            let image = UIImage(data: imageData)
            
            cell.usrImgLbl.image = image
            
            
        }
        
        
        
        
        //imageView.kf.setImage(with: url)

        
        //gdsgdgdfgfdgdgdfgfdgdfgdfgfd
        if dict["Start_time"] != nil {
            
            let Sdate: NSDate? = dateFormatterGet.date(from: dict["Start_time"] as? String ?? "00:00:00")! as NSDate
            let Edate: NSDate? = dateFormatterGet.date(from: dict["end_time"] as? String ?? "00:00:00")! as NSDate
            cell.StimeLbl.text = "\(dateFormatterPrint.string(from: Sdate! as Date))"
            cell.eTimeLbl.text = "\(dateFormatterPrint.string(from: Edate! as Date))"
            
            
            //GymImage.sd_setImage(with: URL(string: (gymData["gym_image"] as? String)!), placeholderImage: UIImage(named: "logo"), options:SDWebImageOptions.refreshCached)

            
            //---------------made by suresh------------------//
            
            let Rmins: Int = abs(Int(CFDateGetTimeIntervalSinceDate(Cdate, Edate))/60)
            let (h,m, s) = secondsToMinutesSeconds(seconds: Rmins*60)
            cell.timeleftLbl.text = String(format: "%02dh %02dm %02ds",h,m,s)

        } else {
            cell.StimeLbl.text = "";
            cell.eTimeLbl.text = "";
        }

        cell.StatusLbl.text = dict["status"] as? String
        
        if (dict["status"] as? String)?.lowercased() == "pending" {
            cell.StatusLbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        } else if (dict["status"] as? String)?.lowercased() == "accepted" || (dict["status"] as? String)?.lowercased() == "ongoing"  {
            cell.StatusLbl.textColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        }else if (dict["status"] as? String)?.lowercased() == "cancelled" || (dict["status"] as? String)?.lowercased() == "rejected" {
            cell.StatusLbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        }
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        let dict:NSDictionary = FilteredTransactionlist.object(at: indexPath.row) as! NSDictionary
        if (dict["status"] as? String)?.lowercased() == "pending" {
            confirm(dict: dict);
            
        } else if (dict["status"] as? String)?.lowercased() == "accepted" || (dict["status"] as? String)?.lowercased() == "ongoing"  {

        }
        
    }
    
    func confirm(dict: NSDictionary) {
        
        let alertController = UIAlertController(title: "Approve or Decline", message: "", preferredStyle: .actionSheet)
        
        
        let saveAction = UIAlertAction(title: "Accept", style: .default, handler: { alert -> Void in
            
            let postdata:NSDictionary = NSDictionary(objects: [dict["transaction_id"] ?? ""], forKeys: ["transaction_id" as NSCopying])
            
            let respData = Appcontrol.common_http_request("/gym/accept-user", value: postdata.jsonRepresentation(), type:POST_METHOD, isjson: true,loader: true)
            
            
            
            
            
            
            
            
            
            
            
            if respData.jsonValue() != nil {
                if let dataDictionary: NSDictionary = respData.jsonValue()! as? NSDictionary{
                    print(dataDictionary.description)
                    if let code:Int = dataDictionary.value(forKey: "status") as? Int {
                        if code == 200 {
                            let refundDictionary: NSDictionary = dataDictionary.value(forKey: "data") as! NSDictionary
                            print(refundDictionary.description);
                            
                            Appcontrol.refresh_user_hours()

                        }
                    }
                }
            }
            Appcontrol.refresh_user_hours()

            
        })
        
        let declineAction = UIAlertAction(title: "Decline", style: .destructive, handler: { alert -> Void in
            
            let postdata:NSDictionary = NSDictionary(objects: [dict["transaction_id"] ?? ""], forKeys: ["transaction_id" as NSCopying])
            
            let respData = Appcontrol.common_http_request("/gym/reject-user", value: postdata.jsonRepresentation(), type:POST_METHOD, isjson: true,loader: true)
            
            if respData.jsonValue() != nil {
                if let dataDictionary: NSDictionary = respData.jsonValue()! as? NSDictionary{
                    print(dataDictionary.description)
                    if let code:Int = dataDictionary.value(forKey: "status") as? Int {
                        if code == 200 {
                            let refundDictionary: NSDictionary = dataDictionary.value(forKey: "data") as! NSDictionary
                            print(refundDictionary.description);
                            Appcontrol.refresh_user_hours()
                        }
                    }
                }
            }
            
            Appcontrol.refresh_user_hours()

        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(saveAction)
        alertController.addAction(declineAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }

    
    
        //---------------changes made By suresh-----------------//
        func secondsToMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
            return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        }
        
        
    

    
}
