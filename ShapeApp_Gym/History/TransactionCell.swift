//
//  TransactionCell.swift
//  ShapeApp
//
//  Created by Satheesh Kumar on 20/01/19.
//  Copyright © 2019 UniqueAngels. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {
    
    @IBOutlet weak var GymnameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var StimeLbl: UILabel!
    @IBOutlet weak var EtimeLbl: UILabel!
    @IBOutlet weak var HrLbl: UILabel!
    @IBOutlet weak var PtsLbl: UILabel!
    @IBOutlet weak var StatusLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
