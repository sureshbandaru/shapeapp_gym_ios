//
//  TransactionVC.swift
//  ShapeApp
//
//  Created by Satheesh Kumar on 20/01/19.
//  Copyright © 2019 UniqueAngels. All rights reserved.
//

import UIKit

class TransactionVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var TransactionTable: UITableView!
    @IBOutlet weak var MonthTxtFld: UITextField!
    var Transactionlist:NSArray = [];
    let dateFormatterGet = DateFormatter()
    let dateFormatterPrint = DateFormatter()
    @IBOutlet weak var pickerView: UIView!
    @IBOutlet weak var noTransactionLbl: UILabel!
    @IBOutlet weak var fromDate: UITextField!
    @IBOutlet weak var toDate: UITextField!
    @IBOutlet weak var TotalPts: UILabel!
    @IBOutlet weak var TotalHrs: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dateFormatterGet.dateFormat = "HH:mm:ss"
        dateFormatterPrint.dateFormat = "hh:mm a"
        
        self.title = "Transaction History"
        let barButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(dismissVC))
        barButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = barButtonItem;
        dp(fromDate)
        dp(toDate)
        
        noTransactionLbl.text = "Please select a period to view transactions"
        
        
        
//        dateFormatterGet.dateFormat = "HH:mm:ss"
//        dateFormatterPrint.dateFormat = "hh:mm a"
//        
//        self.title = "Transaction History"
//        let barButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(dismissVC))
//        barButtonItem.tintColor = UIColor.red
//        self.navigationItem.rightBarButtonItem = barButtonItem;
//        dp(fromDate)
//        dp(toDate)
//        
//        noTransactionLbl.text = "Please select a period to view transactions"
//        
        
        
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    @objc func dismissVC() {
        self.dismiss(animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    @IBAction func HidePicker(_ sender: Any) {
        pickerView.isHidden = true;
    }
    
    @IBAction func changeMonth(_ sender: Any) {
        pickerView.isHidden = false;
    }
    
    @IBAction func dp(_ sender: UITextField) {
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.tag = sender.tag
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        if sender.tag == 0 {
            fromDate.text = dateFormatter.string(from: sender.date)
        } else {
            toDate.text = dateFormatter.string(from: sender.date)
        }
        if fromDate.text?.count ?? 0 > 0 && toDate.text?.count ?? 0 > 0 {
            getPackages(from: fromDate.text!, to: toDate.text!)
        }
        
        fromDate.resignFirstResponder()
        toDate.resignFirstResponder()
        
    }
    
    
    func getPackages(from: String,to: String) {
        
        //let respData = Appcontrol.common_http_request("/gym/history", value: "{\"start_date\":\"\(from))\",\"end_date\":\"\(to)\"}", type:POST_METHOD, isjson: true, loader: true)
        //--------------------------changes made by suresh--------------------------------//
        
        let respData = Appcontrol.common_http_request("/gym/history", value: "{\"start_date\":\"\(from)\",\"end_date\":\"\(to)\",\"gym_id\":\"\("1"))\"}", type:POST_METHOD, isjson: true, loader: true)
        
        
        
        
        if respData.jsonValue() != nil {
            if let userDetailsDictionary: NSDictionary = respData.jsonValue()! as? NSDictionary{
                
                if let code:Int = userDetailsDictionary.value(forKey: "status") as? Int {
                    if code == 200 {
                        Transactionlist = userDetailsDictionary.value(forKey: "data") as! NSArray
                        print("Transaction Array",Transactionlist)
                        
                        
                        var total_hrs = 0
                        var total_pts = 0
                        for item in Transactionlist { // loop through data items
                            let obj = item as! NSDictionary
                            total_hrs += Int("\(obj["Hours"] ?? "0")") ?? 0
                            total_pts += Int("\(obj["Points"] ?? "0")") ?? 0
                        }
                        TotalHrs.text = "\(total_hrs)"
                        TotalPts.text = "\(total_pts)"
                        
                        print("Total Hrs:\(total_hrs)")
                        print("Total Pts:\(total_pts)")
                        
                        if Transactionlist.count > 0 {
                            noTransactionLbl.isHidden = true
                            TransactionTable.isHidden = false
                            TransactionTable.reloadData()
                        } else {
                            TransactionTable.isHidden = true
                            noTransactionLbl.isHidden = false
                            noTransactionLbl.text = "No Transaction Found"
                            
                        }
                    }
                }
            }
            
        }
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Transactionlist.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // create a new cell if needed or reuse an old one
        let cell:TransactionCell = self.TransactionTable.dequeueReusableCell(withIdentifier: "TransactionCell") as! TransactionCell
        
        let dict:NSDictionary = Transactionlist.object(at: indexPath.row) as! NSDictionary
        cell.GymnameLbl.text = dict["name"] as? String
        cell.HrLbl.text = (dict["Hours"] as? String ?? "0")
        cell.PtsLbl.text = (dict["Points"] as? String ?? "0")
        cell.dateLbl.text = dict["date"] as? String
        
        let Sdate: NSDate? = dateFormatterGet.date(from: dict["Start_time"] as? String ?? "00:00:00")! as NSDate
        let Edate: NSDate? = dateFormatterGet.date(from: dict["end_time"] as? String ?? "00:00:00")! as NSDate
        cell.StimeLbl.text = "\(dateFormatterPrint.string(from: Sdate! as Date)) - \(dateFormatterPrint.string(from: Edate! as Date))"
        cell.StatusLbl.text = dict["status"] as? String
        
        if (dict["status"] as? String)?.lowercased() == "pending" {
            cell.StatusLbl.textColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        } else if (dict["status"] as? String)?.lowercased() == "accepted" || (dict["status"] as? String)?.lowercased() == "ongoing"  {
            cell.StatusLbl.textColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
        }else if (dict["status"] as? String)?.lowercased() == "cancelled" || (dict["status"] as? String)?.lowercased() == "rejected" {
            cell.StatusLbl.textColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
        }
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
        
    }
    
}

extension Date {
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM YYYY"
        return dateFormatter.string(from: self)
    }
}
