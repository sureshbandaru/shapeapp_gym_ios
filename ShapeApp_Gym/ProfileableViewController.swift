//
//  ProfileableViewController.swift
//  ShapeApp
//
//  Created by Satheesh Kumar on 08/12/18.
//  Copyright © 2018 UniqueAngels. All rights reserved.
//

import UIKit

class ProfileableViewController: UITableViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profilenameLbl: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "mainbg")!)
        
        self.tableView.contentInset = UIEdgeInsets(top: -20, left: 0, bottom: 0, right: 0)
        
    
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let userdata = Appcontrol.getuserdata();
        profileImage.image = UIImage(named: "\(userdata["gender"] ?? "male")")
        profilenameLbl.text = "\(userdata["first_name"] ?? "Guest") \(userdata["last_name"] ?? "")"
        
    }

    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }

    
    @IBAction func logout(_ sender: Any) {
        
        let alert = UIAlertController.init(title: "Are you sure to Logout?", message: "", preferredStyle:.alert)
        let logout = UIAlertAction.init(title: "Logout", style: .destructive, handler: { (action:UIAlertAction) in
            
            Appcontrol.logout()
            
        })
        let cancel = UIAlertAction(title: "Cancel", style: .default) { (action:UIAlertAction) in
        }
        alert.addAction(logout)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)

    }
    
    @IBAction func transactionhistory(_ sender: Any) {
        
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC: TransactionVC = storyBoard.instantiateViewController(withIdentifier:"TransactionVC") as! TransactionVC
        
        let navigationController: UINavigationController? = UINavigationController(rootViewController: rootVC)
        self.present(navigationController!, animated: true, completion: {
            
        })

    }

    @IBAction func changePassword(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Change Password", message: "", preferredStyle: .alert)
        
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Old Password"
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "New Password"
        }
        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Confirm New Password"
        }
        
        let saveAction = UIAlertAction(title: "Change", style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
            let thirdTextField = alertController.textFields![2] as UITextField
            if (firstTextField.text?.count)! > 0 && (secondTextField.text?.count)! > 0 && (thirdTextField.text?.count)! > 0  {
                print(firstTextField.text ?? "",secondTextField.text ?? "",thirdTextField.text ?? "")
            }
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {
            (action : UIAlertAction!) -> Void in })
        
        alertController.addAction(cancelAction)
        alertController.addAction(saveAction)
        self.present(alertController, animated: true, completion: nil)

    }
    
    @IBAction func sync(_ sender: Any) {
        Appcontrol.refresh_user_data();
        Appcontrol.refresh_gym_data();
        Appcontrol.refresh_user_hours();
        Appcontrol.refresh_onGoing_hours();
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 5
    }

    
    
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
