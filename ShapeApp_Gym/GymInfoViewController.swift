//
//  GymInfoViewController.swift
//  ShapeApp
//
//  Created by Satheesh Kumar on 09/12/18.
//  Copyright © 2018 UniqueAngels. All rights reserved.
//

import UIKit

class GymInfoViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate {
    
    var gymData: NSDictionary = [:]
    var gymId: Int = 0;

    
    @IBOutlet weak var GymNameLbl: UILabel!
    @IBOutlet weak var GymName2Lbl: UILabel!
    @IBOutlet weak var GymImage: UIImageView!
    @IBOutlet weak var gymTypeLbl: UILabel!
    @IBOutlet weak var gymStarLbl: UILabel!
    @IBOutlet weak var GymBannerImage: UIImageView!
    
    @IBOutlet weak var GymAddressTV: UITextView!
    @IBOutlet weak var GymAboutTV: UITextView!
    
    @IBOutlet weak var AboutTVhtCnst: NSLayoutConstraint!
    @IBOutlet weak var AddressTVhtCnst: NSLayoutConstraint!
    var imagelist:NSMutableArray = [];
    
    @IBOutlet weak var imageCollectionViewHtCont: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        let barButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action:#selector(dismissVC))
        barButtonItem.tintColor = UIColor.red
        self.navigationItem.rightBarButtonItem = barButtonItem;
        
        gymData = Appcontrol.getgymdata() as NSDictionary
        
        if gymData != nil {
            self.title = gymData["gym_name"] as? String
            GymNameLbl.text = gymData["gym_name"] as? String
            GymName2Lbl.text = gymData["gym_name"] as? String

            
            gymTypeLbl.text = gymData["gym_category"] as? String
            //GymDescription.text = gymData["about"] as? String
            
//            GymImage.sd_setImage(with: URL(string: (gymData["gym_image"] as? String)!), placeholderImage: UIImage(named: "logo"), options:SDWebImageOptions.refreshCached)
//
            
            
            
                    GymImage.sd_setImage(with: URL(string: (gymData["gym_image"] as? String)!), placeholderImage: UIImage(named: "logo"), options:SDWebImageOptions.refreshCached)
                    GymBannerImage.sd_setImage(with: URL(string: (gymData["banner_image"] as? String)!), placeholderImage: UIImage(named: "gym_banner"), options:SDWebImageOptions.refreshCached)
            
             gymStarLbl.text = String(repeating: "⭐", count: gymData["rating_point"] as? Int ?? 0)
            
            
            
            
            GymAddressTV.text = "Distance:\n\n\(gymData["distance"] as? String ?? "")Address:\n\n\(gymData["address"] as? String ?? "")\n\(gymData["location"] as? String ?? "") - \(gymData["zip_code"] as? String ?? "")\n\nPhone: \(gymData["phone_no"] as? String ?? "")"
                    GymAboutTV.text = gymData["about"] as? String
            
                    let sizeThatFitsTextView = GymAboutTV.sizeThatFits(CGSize(width: GymAboutTV.frame.size.width, height: CGFloat(MAXFLOAT)))
                    AboutTVhtCnst.constant = sizeThatFitsTextView.height
            
                    let sizeThatFitsAddressView = GymAddressTV.sizeThatFits(CGSize(width: GymAddressTV.frame.size.width, height: CGFloat(MAXFLOAT)))
                    AddressTVhtCnst.constant = sizeThatFitsAddressView.height
            
            
            imagelist.removeAllObjects()
            
                    for var i in (0...5) {
                        if let name: String = gymData["image\(i)"] as? String {
                            if name.count > 0 {
                                imagelist.add(name)
                            }
                        }
                    }
                    if imagelist.count > 0 {
                        imageCollectionViewHtCont.constant = 200
                    }
            
        }
        
        
        
        
        
        
//        self.navigationController?.isNavigationBarHidden = true;
//
//        GymName2Lbl.text = ""
//        self.title = gymData["gym_name"] as? String
//        GymNameLbl.text = gymData["gym_name"] as? String
//        gymTypeLbl.text = gymData["gym_category"] as? String
//
//
//
//
//        //GymImage.sd_setImage(with: URL(string: (gymData["gym_image"] as? String)!), placeholderImage: UIImage(named: "logo"), options:SDWebImageOptions.refreshCached)
//        //GymBannerImage.sd_setImage(with: URL(string: (gymData["banner_image"] as? String)!), placeholderImage: UIImage(named: "gym_banner"), options:SDWebImageOptions.refreshCached)
//
//
//
//
//
//
//        gymStarLbl.text = String(repeating: "⭐", count: gymData["rating_point"] as? Int ?? 0)
//
//        GymAddressTV.text = "Distance:\n\n\(gymData["distance"] as? String ?? "")Address:\n\n\(gymData["address"] as? String ?? "")\n\(gymData["location"] as? String ?? "") - \(gymData["zip_code"] as? String ?? "")\n\nPhone: \(gymData["phone_no"] as? String ?? "")"
//        GymAboutTV.text = gymData["about"] as? String
//
//        let sizeThatFitsTextView = GymAboutTV.sizeThatFits(CGSize(width: GymAboutTV.frame.size.width, height: CGFloat(MAXFLOAT)))
//        AboutTVhtCnst.constant = sizeThatFitsTextView.height
//
//        let sizeThatFitsAddressView = GymAddressTV.sizeThatFits(CGSize(width: GymAddressTV.frame.size.width, height: CGFloat(MAXFLOAT)))
//        AddressTVhtCnst.constant = sizeThatFitsAddressView.height
//
//        imagelist.removeAllObjects()
//
//        for var i in (0...5) {
//            if let name: String = gymData["image\(i)"] as? String {
//                if name.count > 0 {
//                    imagelist.add(name)
//                }
//            }
//        }
//        if imagelist.count > 0 {
//            imageCollectionViewHtCont.constant = 200
//        }
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = true;
    }
    
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    
    @IBAction func dismissVC(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func getDirections(_ sender: Any) {
        print("comgooglemaps://?saddr=Google+Inc,+8th+Avenue,+New+York,+NY&daddr=John+F.+Kennedy+International+Airport,+Van+Wyck+Expressway,+Jamaica,+New+York&directionsmode=transit")
        UIApplication.shared.open(URL(string:"comgooglemaps://?daddr=\(gymData["lat"] as? String ?? "0"),\(gymData["lon"] as? String ?? "0")&directionsmode=transit")!, options: [:], completionHandler: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.bounds.intersects(GymNameLbl.frame) {
            self.GymName2Lbl.alpha = 0;
            self.GymName2Lbl.text = ""
        } else {
            if GymName2Lbl.alpha < 1 {
                GymName2Lbl.alpha = 0;
                self.GymName2Lbl.alpha = 1;
                self.GymName2Lbl.text = self.GymNameLbl.text
            }
        }
    }
    
}


extension GymInfoViewController {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return imagelist.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let CVC:ImageGalleryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageGalleryCell", for: indexPath) as! ImageGalleryCell
        let url = imagelist.object(at: indexPath.row) as! String
        CVC.imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "NoImage"), options:SDWebImageOptions.refreshCached)
        return CVC;
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
    }
    
    //    func collectionView(_ collectionView: UICollectionView,
    //                        layout collectionViewLayout: UICollectionViewLayout,
    //                        sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        let kWhateverHeightYouWant = 200
    //        return CGSize(width: collectionView.bounds.size.width/2-5, height: CGFloat(kWhateverHeightYouWant))
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2.5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsets(top: 2.5, left: 2.5, bottom: 2.5, right: 2.5)
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let storyBoard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let rootVC: imageViewController = storyBoard.instantiateViewController(withIdentifier:"imageViewController") as! imageViewController
        rootVC.imagelist = imagelist
        self.navigationController?.pushViewController(rootVC, animated: true)
        
        
    }
}
